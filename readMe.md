# Securite Sociale

Ce projet implémente une base de données pour la gestion de la Sécurité Sociale. Il contient des tables pour les entreprises, les salariés, les retraites, les acteurs médicaux, les prestations médicales et les quartiers prioritaires de la ville (QPVs).

## Configuration requise

- Système de gestion de base de données : MySQL
- Langage de programmation : SQL

## Instructions

1. Démarrez les conteneurs Docker en utilisant la commande suivante :
    ```
    docker compose up -d
    ```
2. Accédez à phpMyAdmin via votre navigateur web en utilisant l'URL suivante : [http://localhost:8080](http://localhost:8080).

3. Connectez-vous à phpMyAdmin en utilisant les informations suivantes :
   
   - Nom d'utilisateur : `root`
   - Mot de passe : `password`

4. Generer de la data grace a la procedure generateData() qui va appeler toutes les procédures listés ci-dessous
5. Vous pouvez aussi simplement importer le fichier init.sql dans votre base si vous ne voulez pas passer par docker
6. Un dump de la base est dispo `dump.sql`

### Procédures stockées

- `InsertEntreprises` : Insère des enregistrements d'entreprises dans la table `Entreprises`.
- `InsertSalaries` : Insère des enregistrements de salariés dans la table `Salaries`.
- `InsertRetraite` : Insère des enregistrements de retraites dans la table `Retraites`.
- `InsertActeursMedicaux` : Insère des enregistrements d'acteurs médicaux dans la table `ActeursMedicaux`.
- `InsertPrestationsMedicales` : Insère des enregistrements de prestations médicales dans la table `PrestationsMedicales`.
- `InsertQPV` : Insère des enregistrements de quartiers prioritaires de la ville (QPVs) dans la table `QPVs`.

### Structure de la base de données

![](schema.png)

La base de données se compose des tables suivantes :

- `Entreprises` : Stocke les informations sur les entreprises.
- `Salaries` : Stocke les informations sur les salariés et leur relation avec les entreprises.
- `Retraites` : Stocke les informations sur les retraites.
- `ActeursMedicaux` : Stocke les informations sur les acteurs médicaux.
- `PrestationsMedicales` : Stocke les informations sur les prestations médicales fournies aux salariés.
- `QPVs` : Stocke les informations sur les quartiers prioritaires de la ville.

Chaque table est liée aux autres tables par des clés étrangères pour établir les relations appropriées entre les entités.

### Requetes

Pour avoir les differentes requetes veuillez utilisez les commandes situé dans `request.sql` pour importer toutes les fonctions necessaires.
Importer egalement les differentes vue via le fichier 'view.sql'.
Il peut y avoir besoin de cette commande pour enregistrer les fonctions : 
```
SET GLOBAL log_bin_trust_function_creators = 1;
```


1. Calculer la balance du système de sécurité sociale
    ```sql
    SELECT CalculerBalanceSecuriteSociale() AS Balance;
    ```

2. Calculer la cotisation de chaque salarié en rapport avec ses prises en charge
    ```sql
    SELECT CalculerCotisationPriseEnCharge(69) AS Cotisation;
    ```

3. Calculer le total financier par type d'acteurs médicaux
    ```sql
    SELECT * FROM TotalFinancierParTypeActeur;
    ```
4. Calculer la moyenne des prestations médicales des retraités ayant une prise en charge de 100%
    ```sql
    SELECT * FROM MoyennePrestationsRetraites100;
    ```

5. Région avec le plus de consommation de prestations
    ```sql
    SELECT * FROM VueRegionConsommation;
    ```

6. Région avec le plus de cotisations intégrées
    ```sql
    SELECT * FROM VueRegionCotisations;
    ```

7. Insérer les QPV pour qualifier les adresses via du geofencing
    ```sql
    SELECT * FROM QualificationAdresses;
    ```


8. Les 3 types de prestation les plus fréquentes dans le système
    ```sql
    SELECT * FROM Top3Prestations;
    ```

9. Les 3 types de prestation ayant coûté le plus cher à la Sécurité Sociale
    ```sql
    SELECT * FROM `Top3PrestationsCout`
    ```

10. Ensemble des prestations médicales par QPV avec leur coût total pour la Sécurité Sociale
    ```sql
    SELECT * FROM `PrestationsParQPV`
    ```
### [Presentation](https://1drv.ms/p/s!AmBTQ6ngs4kNiBvhfJHeGv4L62Wp?e=Xx5Kp7) 


## Auteurs

- Abdou Samath DIOUF
- Kevin Patricello
- Millet Guillaume

