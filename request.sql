-- Fonction pour calculer le total des produits
DELIMITER //
CREATE FUNCTION CalculerTotalProduits() RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE totalProduits DECIMAL(10, 2);

    SELECT SUM(s.salaire_brut * 0.159) INTO totalProduits
    FROM salarie s;

    RETURN totalProduits;
END //
DELIMITER ;

-- Fonction pour calculer le total des charges
DELIMITER //
CREATE FUNCTION CalculerTotalCharges() RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE totalCharges DECIMAL(10, 2);

    SELECT SUM(am.cout_par_patient * am.pourcentage_prise_en_charge) INTO totalCharges
    FROM prestation_medicale pm
    INNER JOIN acteur_medical am ON pm.acteur_medical_id = am.id;

    RETURN totalCharges;
END //
DELIMITER ;

-- Fonction principale pour calculer la balance
DELIMITER //
CREATE FUNCTION CalculerBalanceSecuriteSociale() RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE totalProduits DECIMAL(10, 2);
    DECLARE totalCharges DECIMAL(10, 2);
    DECLARE balance DECIMAL(10, 2);

    SET totalProduits = CalculerTotalProduits();
    SET totalCharges = CalculerTotalCharges();

    SET balance = totalProduits - totalCharges;

    RETURN balance;
END //
DELIMITER ;

DELIMITER //

CREATE FUNCTION CalculerSalaireNet(salarieID INT) RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE salaireNet DECIMAL(10, 2);
    DECLARE tauxCotisation DECIMAL(5, 2);

    -- Get the salaire_brut and cotisation_salariale_id of the employee
    SELECT salaire_brut, cotisation_salariale_id INTO salaireNet, tauxCotisation
    FROM salarie
    WHERE personne_id = salarieID;

    -- Get the montant (rate) based on the cotisation_salariale_id
    SELECT montant INTO tauxCotisation
    FROM cotisation_salariale
    WHERE id = tauxCotisation;

    -- Calculate the net salary based on the rate and salaire_brut
    SET salaireNet = salaireNet - (salaireNet * tauxCotisation / 100);

    RETURN salaireNet;
END //

DELIMITER ;

DELIMITER //

CREATE FUNCTION CalculerCotisationSalariale(salarieID INT) RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE salaireBrut DECIMAL(10, 2);
    DECLARE tauxCotisation DECIMAL(5, 2);
    DECLARE cotisation DECIMAL(10, 2);

    -- Récupérer le salaire brut et l'ID de la cotisation salariale du salarié
    SELECT salaire_brut, cotisation_salariale_id INTO salaireBrut, tauxCotisation
    FROM salarie
    WHERE personne_id = salarieID;

    -- Récupérer le montant (taux) en fonction de l'ID de la cotisation salariale
    SELECT montant INTO tauxCotisation
    FROM cotisation_salariale
    WHERE id = tauxCotisation;

    -- Calculer la cotisation en fonction du taux et du salaire brut
    SET cotisation = salaireBrut * tauxCotisation / 100;

    RETURN cotisation;
END //

DELIMITER ;

DELIMITER //

CREATE FUNCTION CalculerCotisationPriseEnCharge(salarieID INT) RETURNS DECIMAL(10, 2)
BEGIN
    DECLARE salaireBrut DECIMAL(10, 2);
    DECLARE tauxCotisation DECIMAL(5, 2);
    DECLARE priseEnCharge DECIMAL(5, 2);
    DECLARE cotisation DECIMAL(10, 2);

    -- Récupérer le salaire brut, l'ID de la cotisation salariale et la prise en charge du salarié
    SELECT salaire_brut, cotisation_salariale_id, pourcentage_prise_en_charge INTO salaireBrut, tauxCotisation, priseEnCharge
    FROM salarie
    INNER JOIN acteur_medical ON salarie.entreprise_id = acteur_medical.id
    INNER JOIN prestation_medicale ON salarie.personne_id = prestation_medicale.personne_id
    WHERE salarie.personne_id = salarieID;

    -- Récupérer le montant (taux) en fonction de l'ID de la cotisation salariale
    SELECT montant INTO tauxCotisation
    FROM cotisation_salariale
    WHERE id = tauxCotisation;

    -- Calculer la cotisation en fonction du taux, de la prise en charge et du salaire brut
    SET cotisation = (salaireBrut * tauxCotisation / 100) * (1 - priseEnCharge / 100);

    RETURN cotisation;
END //

DELIMITER ;
