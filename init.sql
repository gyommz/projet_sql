-- Tables
CREATE TABLE entreprise (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(100),
  capital_social DECIMAL(10, 2)
);

CREATE TABLE qpv (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(100),
  points_polygone POLYGON
);

CREATE TABLE adresse (
  id INT PRIMARY KEY AUTO_INCREMENT,
  adresse_rue VARCHAR(100),
  adresse_ville VARCHAR(100),
  adresse_region VARCHAR(100),
  latitude DECIMAL(10, 8),
  longitude DECIMAL(11, 8),
  qpv_id INT,
  FOREIGN KEY (qpv_id) REFERENCES qpv(id)
);


CREATE TABLE personne (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(100),
  adresse_id INT,
  FOREIGN KEY (adresse_id) REFERENCES adresse(id)
);

CREATE TABLE cotisation_salariale (
  id INT PRIMARY KEY AUTO_INCREMENT,
  montant DECIMAL(10, 2),
  statut_travail ENUM('cadre', 'non cadre')
);

CREATE TABLE salarie (
  personne_id INT PRIMARY KEY,
  salaire_brut DECIMAL(10, 2),
  entreprise_id INT,
  cotisation_salariale_id INT,
  FOREIGN KEY (personne_id) REFERENCES personne(id),
  FOREIGN KEY (entreprise_id) REFERENCES entreprise(id),
  FOREIGN KEY (cotisation_salariale_id) REFERENCES cotisation_salariale(id)
);

CREATE TABLE retraite (
  personne_id INT PRIMARY KEY,
  FOREIGN KEY (personne_id) REFERENCES personne(id)
);

CREATE TABLE acteur_medical (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nom VARCHAR(100),
  type_acteur ENUM('hopital', 'generaliste', 'specialiste'),
  acte_medical VARCHAR(100),
  cout_par_patient DECIMAL(10, 2),
  pourcentage_prise_en_charge DECIMAL(5, 2)
);

CREATE TABLE prestation_medicale (
  id INT PRIMARY KEY AUTO_INCREMENT,
  personne_id INT,
  acteur_medical_id INT,
  FOREIGN KEY (personne_id) REFERENCES personne(id),
  FOREIGN KEY (acteur_medical_id) REFERENCES acteur_medical(id)
);



-- SET GLOBAL log_bin_trust_function_creators = 1;
-- procedures
DELIMITER //

CREATE PROCEDURE InsertAdresses(numAdresses INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE adresseRue VARCHAR(100);
  DECLARE adresseVille VARCHAR(100);
  DECLARE adresseRegion VARCHAR(100);
  DECLARE qpvId INT;
  DECLARE latitude DECIMAL(10, 8);
  DECLARE longitude DECIMAL(11, 8);

  WHILE i <= numAdresses DO
    SET adresseRue = CONCAT('Rue ', i);
    SET adresseVille = CONCAT('Ville ', i);
    SET adresseRegion = CONCAT('Région ', i);

    -- Choisir aléatoirement un ID de quartier prioritaire de la ville (qpv)
    SET qpvId = (SELECT id FROM qpv ORDER BY RAND() LIMIT 1);
    
    -- Générer des valeurs aléatoires pour latitude et longitude (à titre d'exemple)
    SET latitude = RAND() * (90 - (-90)) - 90;
    SET longitude = RAND() * (180 - (-180)) - 180;

    INSERT INTO adresse (adresse_rue, adresse_ville, adresse_region, latitude, longitude, qpv_id)
    VALUES (adresseRue, adresseVille, adresseRegion, latitude, longitude, qpvId);
    
    SET i = i + 1;
  END WHILE;

  SELECT CONCAT('Inserted ', numAdresses, ' records into adresse table.') AS Result;
END //

CREATE PROCEDURE InsertPersonne(numPersonnes INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE nom VARCHAR(100);
  DECLARE adresseID INT;
  DECLARE personneID INT;

  WHILE i <= numPersonnes DO
    SET nom = CONCAT('Personne ', (SELECT COUNT(*) FROM personne) + 1);

    -- Sélectionner une adresse aléatoire parmi toutes les adresses
    SET adresseID = (
      SELECT id
      FROM adresse
      ORDER BY RAND()
      LIMIT 1
    );

    -- Insérer une nouvelle personne
    INSERT INTO personne (nom, adresse_id)
    VALUES (nom, adresseID);

    -- Récupérer l'ID de la personne nouvellement insérée
    SET personneID = LAST_INSERT_ID();

    SET i = i + 1;
  END WHILE;

  SELECT CONCAT('Inserted ', numPersonnes, ' persons into personne table.') AS Result;
END //

CREATE PROCEDURE InsertEntreprises(numEntreprises INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE nom VARCHAR(100);
  DECLARE capitalSocial DECIMAL(10, 2);
  
  WHILE i <= numEntreprises DO
    SET nom = CONCAT('Entreprise ', i);
    SET capitalSocial = ROUND(RAND() * 100000, 2);
    
    INSERT INTO entreprise (Nom, capital_social) VALUES (nom, capitalSocial);
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numEntreprises, ' records into Entreprises table.') AS Result;
END //

CREATE PROCEDURE GenerateCotisationsSalariales()
BEGIN
  DECLARE tauxCotisationCadre DECIMAL(5, 2);
  DECLARE tauxCotisationNonCadre DECIMAL(5, 2);
  
  SET tauxCotisationCadre = 0.2; -- Taux de cotisation pour les salariés cadres
  SET tauxCotisationNonCadre = 0.1; -- Taux de cotisation pour les salariés non cadres
  
  INSERT INTO cotisation_salariale (statut_travail, montant) VALUES ('cadre', tauxCotisationCadre);
  INSERT INTO cotisation_salariale (statut_travail, montant) VALUES ('non cadre', tauxCotisationNonCadre);
  
  SELECT 'Generated cotisation_salariale records.' AS Result;
END //

CREATE PROCEDURE InsertQPV(numQPVs INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE nom VARCHAR(100);
  DECLARE pointsPolygone GEOMETRY;
  
  WHILE i <= numQPVs DO
    SET nom = CONCAT('QPV ', i);
    
    -- Générer des coordonnées aléatoires pour les points du polygone
    SET @lat1 := RAND() * 180 - 90;
    SET @lon1 := RAND() * 360 - 180;
    SET @lat2 := RAND() * 180 - 90;
    SET @lon2 := RAND() * 360 - 180;
    SET @lat3 := RAND() * 180 - 90;
    SET @lon3 := RAND() * 360 - 180;
    SET @lat4 := RAND() * 180 - 90;
    SET @lon4 := RAND() * 360 - 180;
    
    -- Créer le polygone en utilisant les coordonnées générées
    SET @polygonText := CONCAT(
      'POLYGON((',
      @lat1, ' ', @lon1, ',',
      @lat2, ' ', @lon2, ',',
      @lat3, ' ', @lon3, ',',
      @lat4, ' ', @lon4, ',',
      @lat1, ' ', @lon1,
      '))'
    );
    
    SET pointsPolygone = ST_PolygonFromText(@polygonText);
    
    INSERT INTO qpv (nom, points_polygone)
    VALUES (nom, pointsPolygone);
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numQPVs, ' records into qpv table.') AS Result;
END //



CREATE PROCEDURE InsertSalaries(numSalaries INT, entrepriseID INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE nom VARCHAR(100);
  DECLARE salaireBrut DECIMAL(10, 2);
  DECLARE statutTravail ENUM('cadre', 'non cadre');
  DECLARE adresseID INT;
  DECLARE personneID INT;
  
  WHILE i <= numSalaries DO
    SET nom = CONCAT('Salarié ', i);
    SET salaireBrut = ROUND(RAND() * 10000, 2);
    SET statutTravail = IF(RAND() > 0.5, 'cadre', 'non cadre'); -- Détermine le statut de travail aléatoirement
    
    -- Sélectionner une adresse aléatoire
    SET adresseID = (SELECT id FROM adresse ORDER BY RAND() LIMIT 1);
    
    -- Sélectionner un ID de personne qui n'est pas associé à un salarié ou à un retraité
    SET personneID = (SELECT id FROM personne WHERE id NOT IN (SELECT personne_id FROM salarie UNION SELECT personne_id FROM retraite) ORDER BY RAND() LIMIT 1);
    
    -- Insérer le salarié avec l'entreprise, la personne correspondante et la cotisation salariale
    INSERT INTO salarie (personne_id, salaire_brut, entreprise_id, cotisation_salariale_id)
    VALUES (personneID, salaireBrut, entrepriseID, (SELECT id FROM cotisation_salariale WHERE statut_travail = statutTravail ORDER BY RAND() LIMIT 1));
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numSalaries, ' records into salarie table.') AS Result;
END //

CREATE PROCEDURE InsertActeursMedicaux(numActeurs INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE nom VARCHAR(100);
  DECLARE typeActeur ENUM('Hopital', 'Generaliste', 'Specialiste');
  DECLARE acteMedical VARCHAR(100);
  DECLARE coutParPatient DECIMAL(10, 2);
  DECLARE pourcentagePriseEnCharge DECIMAL(5, 2);
  
  WHILE i <= numActeurs DO
    SET nom = CONCAT('Acteur ', i);
    SET typeActeur = CASE 
      WHEN i % 3 = 0 THEN 'Hopital'
      WHEN i % 3 = 1 THEN 'Generaliste'
      WHEN i % 3 = 2 THEN 'Specialiste'
    END;
    SET acteMedical = CONCAT('Acte ', i);
    SET coutParPatient = ROUND(RAND() * 10000, 2);
    SET pourcentagePriseEnCharge = ROUND(RAND() * 100, 2);
    
    INSERT INTO acteur_medical (nom, type_acteur, acte_medical, cout_par_patient, pourcentage_prise_en_charge)
    VALUES (nom, typeActeur, acteMedical, coutParPatient, pourcentagePriseEnCharge);
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numActeurs, ' records into ActeursMedicaux table.') AS Result;
END //

CREATE PROCEDURE InsertPrestationsMedicales(numPrestations INT, maxSalaries INT, maxActeurs INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE personneID INT;
  DECLARE acteurMedicalID INT;
  
  WHILE i <= numPrestations DO
    SET personneID = FLOOR(RAND() * maxSalaries) + 1; -- Génère un ID de personne (salarié) aléatoire
    SET acteurMedicalID = FLOOR(RAND() * maxActeurs) + 1; -- Génère un ID d'acteur médical aléatoire
    
    INSERT INTO prestation_medicale (personne_id, acteur_medical_id)
    VALUES (personneID, acteurMedicalID);
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numPrestations, ' records into prestation_medicale table.') AS Result;
END //

CREATE PROCEDURE InsertRetraites(numRetraites INT)
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE personneID INT;
  
  WHILE i <= numRetraites DO
    
    -- Trouver un ID disponible dans la table personne
    SET personneID = (
      SELECT MIN(id) 
      FROM (
        SELECT id FROM personne
        EXCEPT
        SELECT personne_id FROM salarie
        EXCEPT
        SELECT personne_id FROM retraite
      ) AS available_ids
    );
    
    INSERT INTO retraite (personne_id)
    VALUES (personneID);
    
    SET i = i + 1;
  END WHILE;
  
  SELECT CONCAT('Inserted ', numRetraites, ' records into Retraites table.') AS Result;
END //

CREATE PROCEDURE GenerateData()
BEGIN
  DECLARE numEntreprises INT;
  DECLARE numSalariesParEntreprise INT;
  DECLARE entrepriseID INT;
  DECLARE maxSalaries INT;

  SET numEntreprises = 10;
  SET numSalariesParEntreprise = 50;
  SET entrepriseID = 1;
  SET maxSalaries = numEntreprises * numSalariesParEntreprise;

  -- Générer les entreprises
  CALL InsertEntreprises(numEntreprises);
  -- Générer les QPV
  CALL InsertQPV(100);
  -- Générer les acteurs médicaux
  CALL InsertActeursMedicaux(20);
  -- Générer les adresses
  CALL InsertAdresses(1000);
  -- Générer les personnes
  CALL InsertPersonne(1000);
  -- Générer les cotisations salariales
  CALL GenerateCotisationsSalariales();

  -- Générer les salariés pour chaque entreprise
  WHILE entrepriseID <= numEntreprises DO
    CALL InsertSalaries(numSalariesParEntreprise, entrepriseID);
    SET entrepriseID = entrepriseID + 1;
  END WHILE;

  -- Générer les prestations médicales
  CALL InsertPrestationsMedicales(5000, maxSalaries, 20);
  -- Générer les retraités
  CALL InsertRetraites(100);

  SELECT 'Data generation completed.' AS Result;
END //

DELIMITER ;
