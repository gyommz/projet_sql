CREATE VIEW SalariesPrisEnCharge AS
SELECT s.personne_id, p.nom
FROM salarie s
INNER JOIN prestation_medicale AS pm ON s.personne_id = pm.personne_id
INNER JOIN personne p ON s.personne_id = p.id;

CREATE VIEW TotalFinancierParTypeActeur AS
SELECT am.type_acteur, SUM(am.cout_par_patient * am.pourcentage_prise_en_charge) AS total_financier
FROM acteur_medical am
GROUP BY am.type_acteur;

CREATE VIEW MoyennePrestationsRetraites100 AS
SELECT AVG(am.cout_par_patient) AS moyenne_prestations
FROM prestation_medicale pm
INNER JOIN acteur_medical am ON pm.acteur_medical_id = am.id
INNER JOIN personne p ON pm.personne_id = p.id
INNER JOIN retraite r ON p.id = r.personne_id
WHERE am.pourcentage_prise_en_charge = 100;


CREATE VIEW VueRegionConsommation AS
SELECT adresse_region, COUNT(*) AS nombre_prestations
FROM adresse a
INNER JOIN personne p ON a.id = p.adresse_id
INNER JOIN prestation_medicale pm ON p.id = pm.personne_id
GROUP BY adresse_region
ORDER BY nombre_prestations DESC
LIMIT 1;

CREATE VIEW VueRegionCotisations AS
SELECT adresse_region, COUNT(*) AS nombre_cotisations
FROM adresse a
INNER JOIN personne p ON a.id = p.adresse_id
INNER JOIN salarie s ON p.id = s.personne_id
WHERE s.cotisation_salariale_id IS NOT NULL
GROUP BY adresse_region
ORDER BY nombre_cotisations DESC
LIMIT 1;

CREATE VIEW Top3Prestations AS
SELECT acte_medical, COUNT(*) AS total_prestations
FROM prestation_medicale
INNER JOIN acteur_medical ON prestation_medicale.acteur_medical_id = acteur_medical.id
GROUP BY acte_medical
ORDER BY total_prestations DESC
LIMIT 3;

CREATE VIEW Top3PrestationsCout AS
SELECT acte_medical, SUM(cout_par_patient * pourcentage_prise_en_charge) AS total_cout
FROM prestation_medicale
INNER JOIN acteur_medical ON prestation_medicale.acteur_medical_id = acteur_medical.id
GROUP BY acte_medical
ORDER BY total_cout DESC
LIMIT 3;

CREATE VIEW QualificationAdresses AS
SELECT a.id, a.adresse_rue, a.adresse_ville, a.adresse_region, q.nom AS qpv_nom
FROM adresse a
JOIN qpv q ON ST_Contains(q.points_polygone, POINT(a.longitude, a.latitude));

CREATE VIEW PrestationsParQPV AS
SELECT q.nom AS qpv_nom, SUM(am.cout_par_patient * (100 - am.pourcentage_prise_en_charge) / 100) AS cout_total_securite_sociale
FROM qpv q
JOIN adresse a ON q.id = a.qpv_id
JOIN prestation_medicale pm ON a.id = pm.personne_id
JOIN acteur_medical am ON pm.acteur_medical_id = am.id
GROUP BY q.nom;